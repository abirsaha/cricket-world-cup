-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 02, 2019 at 07:36 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cwc19`
--

-- --------------------------------------------------------

--
-- Table structure for table `ballsrecord`
--

CREATE TABLE `ballsrecord` (
  `ball_id` int(11) NOT NULL,
  `batsman` int(11) DEFAULT NULL,
  `bolwer` int(11) DEFAULT NULL,
  `ball_num` int(11) DEFAULT NULL,
  `runs` int(11) DEFAULT NULL,
  `is_no` int(11) DEFAULT NULL,
  `iswicket` int(11) DEFAULT NULL,
  `in_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `currentinn`
--

CREATE TABLE `currentinn` (
  `curr_id` int(11) NOT NULL,
  `in_id` int(11) DEFAULT NULL,
  `batting_team` int(11) DEFAULT NULL,
  `bowling_team` int(11) DEFAULT NULL,
  `m_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currentinn`
--

INSERT INTO `currentinn` (`curr_id`, `in_id`, `batting_team`, `bowling_team`, `m_id`) VALUES
(1, 21, 1, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `innings`
--

CREATE TABLE `innings` (
  `in_id` int(11) NOT NULL,
  `batting_team` int(11) DEFAULT NULL,
  `bowling_team` int(11) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `m_id` int(11) DEFAULT NULL,
  `wicket` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `innings`
--

INSERT INTO `innings` (`in_id`, `batting_team`, `bowling_team`, `score`, `m_id`, `wicket`) VALUES
(19, 2, 1, 70, 2, 7),
(20, 5, 4, 4, 5, 1),
(21, 1, 2, 6, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `matches`
--

CREATE TABLE `matches` (
  `m_id` int(11) NOT NULL,
  `m_loc` varchar(225) DEFAULT NULL,
  `m_type` varchar(225) DEFAULT NULL,
  `t1_id` int(11) DEFAULT NULL,
  `t2_id` int(11) DEFAULT NULL,
  `t1_score` int(11) DEFAULT NULL,
  `t2_score` int(11) DEFAULT NULL,
  `t1_wicket` int(11) DEFAULT NULL,
  `t2_wicket` int(11) DEFAULT NULL,
  `m_date` varchar(224) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `matches`
--

INSERT INTO `matches` (`m_id`, `m_loc`, `m_type`, `t1_id`, `t2_id`, `t1_score`, `t2_score`, `t1_wicket`, `t2_wicket`, `m_date`) VALUES
(2, 'sydney stadium', 'ODI', 1, 2, 0, 0, 0, 0, '2019-08-22'),
(3, 'sydney stadium', 'ODI', 1, 3, 0, 0, 0, 0, NULL),
(4, 'sydney stadium', 'ODI', 3, 4, 0, 0, 0, 0, '2019-07-16'),
(5, 'sydney stadium', 'ODI', 4, 5, 0, 0, 0, 0, '2019-07-18'),
(6, 'sydney stadium', 'ODI', 2, 4, 0, 0, 0, 0, '2019-08-22');

-- --------------------------------------------------------

--
-- Table structure for table `newover`
--

CREATE TABLE `newover` (
  `o_id` int(11) NOT NULL,
  `over_num` int(11) DEFAULT NULL,
  `batsman1` int(11) DEFAULT NULL,
  `batsman2` int(11) DEFAULT NULL,
  `bowler` int(11) DEFAULT NULL,
  `in_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `newover`
--

INSERT INTO `newover` (`o_id`, `over_num`, `batsman1`, `batsman2`, `bowler`, `in_id`) VALUES
(1, 1, 6, 6, 5, 20);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `n_id` int(11) NOT NULL,
  `news` varchar(224) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `players`
--

CREATE TABLE `players` (
  `p_id` int(11) NOT NULL,
  `p_name` varchar(225) DEFAULT NULL,
  `p_age` int(11) DEFAULT NULL,
  `p_position` varchar(225) DEFAULT NULL,
  `bat_type` varchar(225) DEFAULT NULL,
  `bowl_type` varchar(225) DEFAULT NULL,
  `fielding` varchar(225) DEFAULT NULL,
  `strike_rate` int(11) DEFAULT NULL,
  `bowling_rate` int(11) DEFAULT NULL,
  `total_match` int(11) DEFAULT NULL,
  `handed` varchar(225) DEFAULT NULL,
  `fiftys` int(11) DEFAULT NULL,
  `hundreds` int(11) DEFAULT NULL,
  `wickets` int(11) DEFAULT NULL,
  `highest_score` int(11) DEFAULT NULL,
  `catches` int(11) DEFAULT NULL,
  `team_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `players`
--

INSERT INTO `players` (`p_id`, `p_name`, `p_age`, `p_position`, `bat_type`, `bowl_type`, `fielding`, `strike_rate`, `bowling_rate`, `total_match`, `handed`, `fiftys`, `hundreds`, `wickets`, `highest_score`, `catches`, `team_id`, `status`) VALUES
(1, 'virat kohili', 27, 'batsman', 'Opening batsman', 'defensive', 'inside', 100, 40, 345, 'righthanded', 72, 41, 0, 153, 56, 2, 1),
(2, 'Masrafi BIn Mortaza', 31, 'bowler', 'Aggresive', 'morderate', 'inside', 50, 87, 445, 'righthanded', 1, 0, 300, 51, 100, 1, 1),
(3, 'mushif ', 29, 'batsman', 'morderate', 'defensive', 'wicket keeper', 100, 0, 300, 'righthanded', 18, 7, 0, 153, 100, 1, 1),
(4, 'N', 31, 'bowler', 'defensive', 'Opening bowler', 'inside', 50, 121, 100, 'righthanded', 0, 0, 3, 12, 0, 4, 1),
(5, 'gourob', 31, 'all-rounder', 'Aggresive', 'Aggresive', 'outside', 50, 87, 100, 'righthanded', 1, 0, 300, 51, 10, 4, 1),
(6, 'tahan', 21, 'batsman', 'Opening batsman', 'morderate', 'wicket keeper', 50, 40, 100, 'righthanded', 72, 0, 0, 51, 100, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `player_inn`
--

CREATE TABLE `player_inn` (
  `p_inn` int(11) NOT NULL,
  `in_id` int(11) DEFAULT NULL,
  `p_id` int(11) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `status` varchar(225) DEFAULT NULL,
  `fielder` int(11) DEFAULT NULL,
  `bowler` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `player_inn`
--

INSERT INTO `player_inn` (`p_inn`, `in_id`, `p_id`, `score`, `status`, `fielder`, `bowler`) VALUES
(1, 20, 6, 4, 'out', 4, 5),
(2, 21, 2, 5, 'not out', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `point`
--

CREATE TABLE `point` (
  `team_id` int(11) DEFAULT NULL,
  `point` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `team_id` int(11) NOT NULL,
  `team_name` varchar(224) DEFAULT NULL,
  `total_wins` int(11) DEFAULT NULL,
  `total_loss` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`team_id`, `team_name`, `total_wins`, `total_loss`) VALUES
(1, 'Bangladesh', 200, 245),
(2, 'India', 120, 145),
(3, 'Australia', 300, 345),
(4, 'England', 325, 100),
(5, 'new zelend', 300, 100);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `created_at`) VALUES
(1, 'tonni', '$2y$10$ofF06TMF7Cxf3WHDhkP4yuTFO1tseqcDafaefIbgig6Kf1jDlHz/.', '2019-07-17 11:11:03'),
(2, 'abir saha', '$2y$10$RS3sXKRGC93SaTKCh5DF0uX4TCWw4wuvAfBMgq04XRVHtcjNhxF8m', '2019-08-22 22:51:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ballsrecord`
--
ALTER TABLE `ballsrecord`
  ADD PRIMARY KEY (`ball_id`),
  ADD KEY `batsman` (`batsman`),
  ADD KEY `bolwer` (`bolwer`);

--
-- Indexes for table `currentinn`
--
ALTER TABLE `currentinn`
  ADD PRIMARY KEY (`curr_id`),
  ADD KEY `in_id` (`in_id`),
  ADD KEY `batting_team` (`batting_team`),
  ADD KEY `bowling_team` (`bowling_team`),
  ADD KEY `m_id` (`m_id`);

--
-- Indexes for table `innings`
--
ALTER TABLE `innings`
  ADD PRIMARY KEY (`in_id`),
  ADD KEY `batting_team` (`batting_team`),
  ADD KEY `bowling_team` (`bowling_team`),
  ADD KEY `m_id` (`m_id`);

--
-- Indexes for table `matches`
--
ALTER TABLE `matches`
  ADD PRIMARY KEY (`m_id`),
  ADD KEY `t1_id` (`t1_id`);

--
-- Indexes for table `newover`
--
ALTER TABLE `newover`
  ADD PRIMARY KEY (`o_id`),
  ADD KEY `batsman1` (`batsman1`),
  ADD KEY `batsman2` (`batsman2`),
  ADD KEY `bowler` (`bowler`),
  ADD KEY `in_id` (`in_id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`n_id`);

--
-- Indexes for table `players`
--
ALTER TABLE `players`
  ADD PRIMARY KEY (`p_id`),
  ADD KEY `team_id` (`team_id`);

--
-- Indexes for table `player_inn`
--
ALTER TABLE `player_inn`
  ADD PRIMARY KEY (`p_inn`),
  ADD KEY `in_id` (`in_id`),
  ADD KEY `p_id` (`p_id`);

--
-- Indexes for table `point`
--
ALTER TABLE `point`
  ADD KEY `team_id` (`team_id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`team_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ballsrecord`
--
ALTER TABLE `ballsrecord`
  MODIFY `ball_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `currentinn`
--
ALTER TABLE `currentinn`
  MODIFY `curr_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `innings`
--
ALTER TABLE `innings`
  MODIFY `in_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `matches`
--
ALTER TABLE `matches`
  MODIFY `m_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `newover`
--
ALTER TABLE `newover`
  MODIFY `o_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `n_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `players`
--
ALTER TABLE `players`
  MODIFY `p_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `player_inn`
--
ALTER TABLE `player_inn`
  MODIFY `p_inn` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `team_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ballsrecord`
--
ALTER TABLE `ballsrecord`
  ADD CONSTRAINT `ballsrecord_ibfk_1` FOREIGN KEY (`batsman`) REFERENCES `players` (`p_id`),
  ADD CONSTRAINT `ballsrecord_ibfk_2` FOREIGN KEY (`bolwer`) REFERENCES `players` (`p_id`);

--
-- Constraints for table `currentinn`
--
ALTER TABLE `currentinn`
  ADD CONSTRAINT `currentinn_ibfk_1` FOREIGN KEY (`in_id`) REFERENCES `innings` (`in_id`),
  ADD CONSTRAINT `currentinn_ibfk_2` FOREIGN KEY (`batting_team`) REFERENCES `teams` (`team_id`),
  ADD CONSTRAINT `currentinn_ibfk_3` FOREIGN KEY (`bowling_team`) REFERENCES `teams` (`team_id`),
  ADD CONSTRAINT `currentinn_ibfk_4` FOREIGN KEY (`m_id`) REFERENCES `matches` (`m_id`);

--
-- Constraints for table `innings`
--
ALTER TABLE `innings`
  ADD CONSTRAINT `innings_ibfk_1` FOREIGN KEY (`batting_team`) REFERENCES `teams` (`team_id`),
  ADD CONSTRAINT `innings_ibfk_2` FOREIGN KEY (`bowling_team`) REFERENCES `teams` (`team_id`),
  ADD CONSTRAINT `innings_ibfk_3` FOREIGN KEY (`m_id`) REFERENCES `matches` (`m_id`);

--
-- Constraints for table `matches`
--
ALTER TABLE `matches`
  ADD CONSTRAINT `matches_ibfk_1` FOREIGN KEY (`t1_id`) REFERENCES `teams` (`team_id`),
  ADD CONSTRAINT `matches_ibfk_2` FOREIGN KEY (`t1_id`) REFERENCES `teams` (`team_id`);

--
-- Constraints for table `newover`
--
ALTER TABLE `newover`
  ADD CONSTRAINT `newover_ibfk_1` FOREIGN KEY (`batsman1`) REFERENCES `players` (`p_id`),
  ADD CONSTRAINT `newover_ibfk_2` FOREIGN KEY (`batsman2`) REFERENCES `players` (`p_id`),
  ADD CONSTRAINT `newover_ibfk_3` FOREIGN KEY (`bowler`) REFERENCES `players` (`p_id`),
  ADD CONSTRAINT `newover_ibfk_4` FOREIGN KEY (`in_id`) REFERENCES `innings` (`in_id`);

--
-- Constraints for table `players`
--
ALTER TABLE `players`
  ADD CONSTRAINT `players_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `teams` (`team_id`);

--
-- Constraints for table `player_inn`
--
ALTER TABLE `player_inn`
  ADD CONSTRAINT `player_inn_ibfk_1` FOREIGN KEY (`in_id`) REFERENCES `innings` (`in_id`),
  ADD CONSTRAINT `player_inn_ibfk_2` FOREIGN KEY (`p_id`) REFERENCES `players` (`p_id`);

--
-- Constraints for table `point`
--
ALTER TABLE `point`
  ADD CONSTRAINT `point_ibfk_1` FOREIGN KEY (`team_id`) REFERENCES `teams` (`team_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
