<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php
// Initialize the session
session_start();
 
// Check if the user is logged in, if not then redirect him to login page
if(!isset($_SESSION["loggedin"]) || $_SESSION["loggedin"] !== true){
    header("location: login.php");
    exit;
}

 
// Include config file
require_once "config.php";
?>




<!DOCTYPE HTML>
<html>
<head>
<title>Glance Design Dashboard an Admin Panel Category Flat Bootstrap Responsive Website Template | Home :: w3layouts</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Glance Design Dashboard Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
SmartPhone Compatible web template, free WebDesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>

<!-- Bootstrap Core CSS -->
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />

<!-- Custom CSS -->
<link href="css/style.css" rel='stylesheet' type='text/css' />

<!-- font-awesome icons CSS -->
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //font-awesome icons CSS-->

<!-- side nav css file -->
<link href='css/SidebarNav.min.css' media='all' rel='stylesheet' type='text/css'/>
<!-- //side nav css file -->
 
 <!-- js-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/modernizr.custom.js"></script>

<!--webfonts-->
<link href="//fonts.googleapis.com/css?family=PT+Sans:400,400i,700,700i&amp;subset=cyrillic,cyrillic-ext,latin-ext" rel="stylesheet">
<!--//webfonts--> 

<!-- chart -->
<script src="js/Chart.js"></script>
<!-- //chart -->

<!-- Metis Menu -->
<script src="js/metisMenu.min.js"></script>
<script src="js/custom.js"></script>
<link href="css/custom.css" rel="stylesheet">
<!--//Metis Menu -->
<style>
#chartdiv {
  width: 100%;
  height: 295px;
}
</style>
<!--pie-chart --><!-- index page sales reviews visitors pie chart -->
<script src="js/pie-chart.js" type="text/javascript"></script>
 <script type="text/javascript">

        $(document).ready(function () {
            $('#demo-pie-1').pieChart({
                barColor: '#2dde98',
                trackColor: '#eee',
                lineCap: 'round',
                lineWidth: 8,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                }
            });

            $('#demo-pie-2').pieChart({
                barColor: '#8e43e7',
                trackColor: '#eee',
                lineCap: 'butt',
                lineWidth: 8,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                }
            });

            $('#demo-pie-3').pieChart({
                barColor: '#ffc168',
                trackColor: '#eee',
                lineCap: 'square',
                lineWidth: 8,
                onStep: function (from, to, percent) {
                    $(this.element).find('.pie-value').text(Math.round(percent) + '%');
                }
            });

           
        });

    </script>
<!-- //pie-chart --><!-- index page sales reviews visitors pie chart -->

	<!-- requried-jsfiles-for owl -->
					<link href="css/owl.carousel.css" rel="stylesheet">
					<script src="js/owl.carousel.js"></script>
						<script>
							$(document).ready(function() {
								$("#owl-demo").owlCarousel({
									items : 3,
									lazyLoad : true,
									autoPlay : true,
									pagination : true,
									nav:true,
								});
							});
						</script>
					<!-- //requried-jsfiles-for owl -->
</head> 
<body class="cbp-spmenu-push">
	<div class="main-content">
	<div class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
		<!--left-fixed -navigation-->
		<aside class="sidebar-left">
      <nav class="navbar navbar-inverse">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".collapse" aria-expanded="false">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <h1><a class="navbar-brand" href="index.html"><span class="fa fa-area-chart"></span> Glance<span class="dashboard_text">Design dashboard</span></a></h1>
          </div>
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="sidebar-menu">
              <li class="header">MAIN NAVIGATION</li>
              <li class="treeview">
                <a href="dashboard.php">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
              </li>
			  <li class="treeview">
                <a href="#">
                <i class="fas fa-cricket"></i>
                <span>Players</span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="addplayer.php"><i class="fa fa-angle-right"></i> Add player</a></li>
                  <li><a href="showplayer"><i class="fa fa-angle-right"></i> Show players</a></li>
                  <li><a href="Search player"><i class="fa fa-angle-right"></i> Search players</a></li>
                </ul>
              </li>
             
              <li class="treeview">
              <li class="treeview">
                <a href="#">
                <i class="fa fa-laptop"></i>
                <span>Teams</span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="addteams.php"><i class="fa fa-angle-right"></i> Add a team</a></li>
                  <li><a href="showteams.php"><i class="fa fa-angle-right"></i> Show teams</a></li>
                  <li><a href="buttons.html"><i class="fa fa-angle-right"></i> search teams</a></li>
                  <li><a href="Squard.php"><i class="fa fa-angle-right"></i>Teams Squard</a></li>
                </ul>
              </li>
			 
              <li class="treeview">
                <a href="#">
                <i class="fa fa-edit"></i> <span>Matches</span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="addmatch.php"><i class="fa fa-angle-right"></i>Add a match</a></li>
                  <li><a href="showmatch.php"><i class="fa fa-angle-right"></i> Show matches</a></li>
                  <li><a href="showmatch.php"><i class="fa fa-angle-right"></i> Search matches</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#">
                <i class="fa fa-table"></i> <span>innigs</span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="addinnings.php"><i class="fa fa-angle-right"></i>New innings</a></li>
                  <li><a href="showinnings.php"><i class="fa fa-angle-right"></i>Show innings</a></li>
                </ul>
              </li>
             
              <li class="treeview">
                <a href="#">
                <i class="fa fa-folder"></i> <span> Overs</span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="newover.php"><i class="fa fa-angle-right"></i>new over </a></li>
                  
                </ul>
              </li>

              <li class="treeview">
                <a href="#">
                <i class="fa fa-folder"></i> <span>Ballsrecord</span>
                <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                  <li><a href="newball.php"><i class="fa fa-angle-right"></i>new ball </a></li>
                   <li><a href="showballrecord.php"><i class="fa fa-angle-right"></i>show ball record</a></li>
                  
                </ul>
              </li>



             
            </ul>
          </div>
          <!-- /.navbar-collapse -->
      </nav>
    </aside>
	</div>
		<!--left-fixed -navigation-->
		
		<!-- header-starts -->
		<div class="sticky-header header-section ">
			<div class="header-left">
				
				<!--toggle button start-->
				<button id="showLeftPush"><i class="fa fa-bars"></i></button>
				<!--toggle button end-->
				<div class="profile_details_left"><!--notifications of menu start -->
					<ul class="nofitications-dropdown">
						<li class="dropdown head-dpdn">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-envelope"></i><span class="badge">4</span></a>
							<ul class="dropdown-menu">
								<li>
									<div class="notification_header">
										<h3>You have 3 new messages</h3>
									</div>
								</li>
								<li><a href="#">
								   <div class="user_img"><img src="images/1.jpg" alt=""></div>
								   <div class="notification_desc">
									<p>Lorem ipsum dolor amet</p>
									<p><span>1 hour ago</span></p>
									</div>
								   <div class="clearfix"></div>	
								</a></li>
								<li class="odd"><a href="#">
									<div class="user_img"><img src="images/4.jpg" alt=""></div>
								   <div class="notification_desc">
									<p>Lorem ipsum dolor amet </p>
									<p><span>1 hour ago</span></p>
									</div>
								  <div class="clearfix"></div>	
								</a></li>
								<li><a href="#">
								   <div class="user_img"><img src="images/3.jpg" alt=""></div>
								   <div class="notification_desc">
									<p>Lorem ipsum dolor amet </p>
									<p><span>1 hour ago</span></p>
									</div>
								   <div class="clearfix"></div>	
								</a></li>
								<li>
									<div class="notification_bottom">
										<a href="#">See all messages</a>
									</div> 
								</li>
							</ul>
						</li>
						<li class="dropdown head-dpdn">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-bell"></i><span class="badge blue">4</span></a>
							<ul class="dropdown-menu">
								<li>
									<div class="notification_header">
										<h3>You have 3 new notification</h3>
									</div>
								</li>
								<li><a href="#">
									<div class="user_img"><img src="images/4.jpg" alt=""></div>
								   <div class="notification_desc">
									<p>Lorem ipsum dolor amet</p>
									<p><span>1 hour ago</span></p>
									</div>
								  <div class="clearfix"></div>	
								 </a></li>
								 <li class="odd"><a href="#">
									<div class="user_img"><img src="images/1.jpg" alt=""></div>
								   <div class="notification_desc">
									<p>Lorem ipsum dolor amet </p>
									<p><span>1 hour ago</span></p>
									</div>
								   <div class="clearfix"></div>	
								 </a></li>
								 <li><a href="#">
									<div class="user_img"><img src="images/3.jpg" alt=""></div>
								   <div class="notification_desc">
									<p>Lorem ipsum dolor amet </p>
									<p><span>1 hour ago</span></p>
									</div>
								   <div class="clearfix"></div>	
								 </a></li>
								 <li>
									<div class="notification_bottom">
										<a href="#">See all notifications</a>
									</div> 
								</li>
							</ul>
						</li>	
						<li class="dropdown head-dpdn">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false"><i class="fa fa-tasks"></i><span class="badge blue1">8</span></a>
							<ul class="dropdown-menu">
								<li>
									<div class="notification_header">
										<h3>You have 8 pending task</h3>
									</div>
								</li>
								<li><a href="#">
									<div class="task-info">
										<span class="task-desc">Database update</span><span class="percentage">40%</span>
										<div class="clearfix"></div>	
									</div>
									<div class="progress progress-striped active">
										<div class="bar yellow" style="width:40%;"></div>
									</div>
								</a></li>
								<li><a href="#">
									<div class="task-info">
										<span class="task-desc">Dashboard done</span><span class="percentage">90%</span>
									   <div class="clearfix"></div>	
									</div>
									<div class="progress progress-striped active">
										 <div class="bar green" style="width:90%;"></div>
									</div>
								</a></li>
								<li><a href="#">
									<div class="task-info">
										<span class="task-desc">Mobile App</span><span class="percentage">33%</span>
										<div class="clearfix"></div>	
									</div>
								   <div class="progress progress-striped active">
										 <div class="bar red" style="width: 33%;"></div>
									</div>
								</a></li>
								<li><a href="#">
									<div class="task-info">
										<span class="task-desc">Issues fixed</span><span class="percentage">80%</span>
									   <div class="clearfix"></div>	
									</div>
									<div class="progress progress-striped active">
										 <div class="bar  blue" style="width: 80%;"></div>
									</div>
								</a></li>
								<li>
									<div class="notification_bottom">
										<a href="#">See all pending tasks</a>
									</div> 
								</li>
							</ul>
						</li>	
					</ul>
					<div class="clearfix"> </div>
				</div>
				<!--notification menu end -->
				<div class="clearfix"> </div>
			</div>
			<div class="header-right">
				
				
				<!--search-box-->
				<div class="search-box">
					<form class="input">
						<input class="sb-search-input input__field--madoka" placeholder="Search..." type="search" id="input-31" />
						<label class="input__label" for="input-31">
							<svg class="graphic" width="100%" height="100%" viewBox="0 0 404 77" preserveAspectRatio="none">
								<path d="m0,0l404,0l0,77l-404,0l0,-77z"/>
							</svg>
						</label>
					</form>
				</div><!--//end-search-box-->
				
				<div class="profile_details">		
					<ul>
						<li class="dropdown profile_details_drop">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
								<div class="profile_img">	
									<span class="prfil-img"><img src="images/2.jpg" alt=""> </span> 
									<div class="user-name">
										<p>Admin Name</p>
										<span><?php echo $_SESSION['username'];?></span>
									</div>
									<i class="fa fa-angle-down lnr"></i>
									<i class="fa fa-angle-up lnr"></i>
									<div class="clearfix"></div>	
								</div>	
							</a>
							<ul class="dropdown-menu drp-mnu">
								<li> <a href="setting.php"><i class="fa fa-cog"></i> Settings</a> </li> 
								<li> <a href="myaccount.php"><i class="fa fa-user"></i> My Account</a> </li>
								<li> <a href="profile.php"><i class="fa fa-suitcase"></i> Profile</a> </li> 
								<li> <a href="logout.php"><i class="fa fa-sign-out"></i> Logout</a> </li>
							</ul>
						</li>
					</ul>
				</div>
				<div class="clearfix"> </div>				
			</div>
			<div class="clearfix"> </div>	
		</div>
		<!-- //header-ends -->
		<!-- main content start-->
		<div id="page-wrapper">
			<div class="main-page">
				<div class="forms">
					<h2 class="title1">Forms</h2>
					<div class="form-grids row widget-shadow" data-example-id="basic-forms"> 
						<div class="form-title">
							<h4>Add A Innings :</h4>
						</div>
						<div class="form-body">
							<form action="addinnings.php" method="POST">
							 <div class="form-group"> 
						
							<!-- team id -->


							 	<div class="form-group"> <label for="exampleInputEmail1">Batting Team :</label> 
                 <select name="batting_team" class="form-control">			
                 <option>select one</option>
                            



							 	<?php
							 	$con= mysqli_connect("localhost","root","","cwc19") or die ("cannot connect to database");

							 	$p= "select * from teams ";
		$run_email= mysqli_query($con,$p);
		while ($row=mysqli_fetch_array($run_email)){


							 	?>
			<option value="<?php echo $row['team_id'];?>"><?php echo $row['team_name'];?></option>
					
                    
                  <?php
						}
                  ?> 
                    
                    
                        </select>
                 </div>



                 



							 	


                 	<div class="form-group"> <label for="exampleInputEmail1">Bowling Team :</label> 
                 <select name="bowling_team" class="form-control">			
                 <option>select one</option>
                            



							 	<?php
							 	$con= mysqli_connect("localhost","root","","cwc19") or die ("cannot connect to database");

							 	$p= "select * from teams ";
		$run_email= mysqli_query($con,$p);
		while ($row=mysqli_fetch_array($run_email)){


							 	?>
			<option value="<?php echo $row['team_id'];?>"><?php echo $row['team_name'];?></option>
					
                    
                  <?php
						}
                  ?> 
                    
                    
                        </select>
                 </div>



                 <div class="form-group"> <label for="exampleInputEmail1">Match id :</label> 
                 <select name="m_id" class="form-control">			
                 <option>select one</option>
                            



							 	<?php
							 	$con= mysqli_connect("localhost","root","","cwc19") or die ("cannot connect to database");

							 	$p= "select * from matches ";
		$run_email= mysqli_query($con,$p);
		while ($row=mysqli_fetch_array($run_email)){


							 	?>
			<option value="<?php echo $row['m_id'];?>"><?php echo $row['m_id']; ?></option>
					
                    
                  <?php
						}
                  ?> 
                    
                    
                        </select>
                 </div>


               

						
                  	 <div class="form-group"> 
							 	
							 	<label for="exampleInputEmail1">Score  : </label>

							 	 <input type="text" class="form-control" id="exampleInputEmail1" placeholder="score" name="score">

							 	 </div>


							 	  <div class="form-group"> 
							 	
							 	<label for="exampleInputEmail1">wicket  : </label>

							 	 <input type="text" class="form-control" id="exampleInputEmail1" placeholder="score" name="wicket">

							 	 </div>



							 	 	<div class="form-group"> 
							 	
							 	


                 
 					



							 	 </div> <button type="submit" class="btn btn-default" name="sub">Submit</button> </form> 
						</div>
					</div>
					
					
				
					
					
						
					</div>
				</div>
			</div>
		
		<!--footer-->
		<div class="footer">
		   <p>&copy; 2018 Glance Design Dashboard. All Rights Reserved | Design by <a href="https://w3layouts.com/" target="_blank">w3layouts</a></p>
	   </div>
        <!--//footer-->
	</div>
	
	<!-- side nav js -->
	<script src='js/SidebarNav.min.js' type='text/javascript'></script>
	<script>
      $('.sidebar-menu').SidebarNav()
    </script>
	<!-- //side nav js -->
	
	<!-- Classie --><!-- for toggle left push menu script -->
		<script src="js/classie.js"></script>
		<script>
			var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
				showLeftPush = document.getElementById( 'showLeftPush' ),
				body = document.body;
				
			showLeftPush.onclick = function() {
				classie.toggle( this, 'active' );
				classie.toggle( body, 'cbp-spmenu-push-toright' );
				classie.toggle( menuLeft, 'cbp-spmenu-open' );
				disableOther( 'showLeftPush' );
			};
			
			function disableOther( button ) {
				if( button !== 'showLeftPush' ) {
					classie.toggle( showLeftPush, 'disabled' );
				}
			}
		</script>
	<!-- //Classie --><!-- //for toggle left push menu script -->
	
	<!--scrolling js-->
	<script src="js/jquery.nicescroll.js"></script>
	<script src="js/scripts.js"></script>
	<!--//scrolling js-->
	
	<!-- Bootstrap Core JavaScript -->
   <script src="js/bootstrap.js"> </script>
   

  
<?php

if(isset($_POST['sub'])){

	$con= mysqli_connect("localhost","root","","cwc19") or die ("cannot connect to database");

	
	echo $m_id=$_POST["m_id"]." ";
	echo $batting_team=$_POST["batting_team"]." ";
	echo $bowling_team=$_POST["bowling_team"]." ";
	
	echo $score=$_POST["score"]." ";
	echo $wicket=$_POST['wicket'];



		$insert ="INSERT INTO innings VALUES(NULL,'$batting_team','$bowling_team','$score','$m_id','$wicket')";

		$run_insert=mysqli_query($con,$insert);
		

		
		
		if ($run_insert)
{
	echo "<script>alert('new innings record added!')</script>";
	// echo "<script>window.open('addinnings.php','_self')</script>";
}
else 
{
	echo "<script>alert('sorry data cannot inserted')</script>";
	exit();
}





							 	$p= "select * from innings where m_id='$m_id' and batting_team='$batting_team' and bowling_team='$bowling_team' ";
		$run_email= mysqli_query($con,$p);
		while ($row=mysqli_fetch_array($run_email)){


							 	
			 $in_id1 =$row['in_id'];
}

		$p1 = "UPDATE currentinn
		SET in_id='$in_id1', batting_team='$batting_team',
		bowling_team='$bowling_team',m_id='$m_id'
		WHERE curr_id=1	 ";

		$run_email1= mysqli_query($con,$p1);

		if($run_email1){
			echo "<script>window.alert('data updated')</script>";
		}
		else{
			echo "data cannot be updated";
		}

}













?>



</body>
</html>